// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

#include "Net/UnrealNetwork.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "GameFramework/Controller.h"
#include "GameFramework/PlayerController.h"

#include "EssentialMaths/EssentialMathsLibrary.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
{
	bReplicates = true;

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	{
		UCharacterMovementComponent* CharMovement = GetCharacterMovement();
		if (CharMovement != nullptr)
		{
			CharMovement->bOrientRotationToMovement = true;
			// CharMovement->bUseControllerDesiredRotation = true;
			CharMovement->bConstrainToPlane = true;
			CharMovement->bSnapToPlaneAtStart = true;
			CharMovement->RotationRate = DefaultRotationRate;

			CharMovement->NavAgentProps.bCanCrouch = true;
			CharMovement->NavAgentProps.bCanFly = true;

			// Settings from ALS-Refactored
			CharMovement->MaxAcceleration = 1500.0f;
			CharMovement->BrakingFrictionFactor = 0.0f;
			CharMovement->bRunPhysicsWithNoController = true;

			CharMovement->GroundFriction = 4.0f;
			CharMovement->MaxWalkSpeed = 600.0f;
			CharMovement->MaxWalkSpeedCrouched = 200.0f;
			CharMovement->MinAnalogWalkSpeed = 25.0f;
			CharMovement->bCanWalkOffLedgesWhenCrouching = true;
			CharMovement->PerchRadiusThreshold = 20.0f;
			CharMovement->PerchAdditionalHeight = 0.0f;
			CharMovement->LedgeCheckThreshold = 0.0f;

			CharMovement->AirControl = 0.15f;
			// https://unrealengine.hatenablog.com/entry/2019/01/16/231404
			CharMovement->FallingLateralFriction = 1.0f;
			CharMovement->JumpOffJumpZFactor = 0.0f;
			CharMovement->bNetworkAlwaysReplicateTransformUpdateTimestamp = true; // Required for view network smoothing.

			CharMovement->bAllowPhysicsRotationDuringAnimRootMotion = true; // Used to allow character rotation while rolling.

			CharMovement->SetJumpAllowed(true);
			CharMovement->SetNetAddressable(); // Make DSO components net addressable
			CharMovement->SetIsReplicated(true); // Enable replication by default
		}
	}

	check(IsValid(GetRootComponent()));
	check(IsValid(GetMesh())); // Mesh - is Torso

	Head = CreateDefaultSubobject<USkeletalMeshComponent>("Head");
	check(IsValid(Head));
	Head->SetupAttachment(GetMesh());

	Helmet = CreateDefaultSubobject<USkeletalMeshComponent>("Helmet");
	check(IsValid(Helmet));
	Helmet->SetupAttachment(GetMesh());

	Cape = CreateDefaultSubobject<USkeletalMeshComponent>("Cape");
	check(IsValid(Cape));
	Cape->SetupAttachment(GetMesh());

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	check(IsValid(SpringArm));

	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->bEnableCameraLag = true;
	SpringArm->TargetArmLength = 800.f;
	SpringArm->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	SpringArm->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	SpringArm->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	check(IsValid(Camera));

	Camera->SetupAttachment(SpringArm);
	Camera->bUsePawnControlRotation = false;

	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ABaseCharacter::AddInputMappingContext_Implementation()
{
	AddInputMappingContextToController();
}

void ABaseCharacter::AddInputMappingContextToController() const
{
	const APlayerController* CharacterPlayerController;

	{
		const AController* CurrentController = GetController();
		if (!IsValid(GetController()))
		{
			return;
		}
		CharacterPlayerController = Cast<APlayerController>(CurrentController);
	}
	
	check(CharacterPlayerController != nullptr);

	UEnhancedInputLocalPlayerSubsystem* InputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(CharacterPlayerController->GetLocalPlayer());
	check(IsValid(InputSubsystem));
	
	InputSubsystem->ClearAllMappings();
	
	check(InputMappingContext != nullptr);
	InputSubsystem->AddMappingContext(InputMappingContext, 0);
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CalcEssentialMovementValues();
}

void ABaseCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	check(Helmet != nullptr);
	check(Head != nullptr);
	check(GetMesh() != nullptr);
	
	Helmet->SetMasterPoseComponent(GetMesh());
	Head->SetMasterPoseComponent(GetMesh());
}

void ABaseCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	AddInputMappingContext();
}

void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	UEnhancedInputComponent* EnhancedInput = Cast<UEnhancedInputComponent>(PlayerInputComponent);

	check(LookAction != nullptr);
	check(MoveAction != nullptr);
	/*
	check(BlockAction != nullptr);
	check(SprintAction != nullptr);
	check(JumpAction != nullptr);
	check(CrouchAction != nullptr);
	check(AttackAction != nullptr);
	check(InventoryAction != nullptr);
	check(InteractAction != nullptr);
	*/

	// EnhancedInput->BindAction(LookAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnLook);
	EnhancedInput->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ThisClass::Input_Move);
	/*
	EnhancedInput->BindAction(BlockAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnBlock);
	EnhancedInput->BindAction(SprintAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnSprint);
	EnhancedInput->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnJump);
	EnhancedInput->BindAction(CrouchAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnCrouch);
	EnhancedInput->BindAction(AttackAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnAttack);
	EnhancedInput->BindAction(InventoryAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnInventory);
	EnhancedInput->BindAction(InteractAction, ETriggerEvent::Triggered, this, &ThisClass::Input_OnInteract);
	*/
}

void ABaseCharacter::Input_OnLook(const FInputActionValue& ActionValue)
{
	const FVector2D Value = ActionValue.Get<FVector2D>();

	AddControllerPitchInput(Value.Y * LookUpMouseSensitivity);
	AddControllerYawInput(Value.X * LookRightMouseSensitivity);
}

void ABaseCharacter::Input_Move(const FInputActionValue& ActionValue)
{
	FRotator Rotation;
	const FVector2D Value = UEssentialMathsLibrary::ClampMagnitude012D(ActionValue.Get<FVector2D>());

	{
		const AController* CurrentController = GetController();
		if (CurrentController == nullptr)
		{
			return;
		}

		Rotation = CurrentController->GetControlRotation();
	}
	
	const FVector ForwardDirection = UEssentialMathsLibrary::AngleToDirection2D(Rotation.Yaw);
	const FVector RightDirection = UEssentialMathsLibrary::RightVectorFromVector(ForwardDirection);

	AddMovementInput(ForwardDirection * Value.Y + RightDirection * Value.X, SpeedScale);
}

void ABaseCharacter::CalcEssentialMovementValues()
{
	float DeltaSeconds, MovementInputAmount;
	FVector CurrentAcceleration;
	bool IsInAir;
	
	{
		const UWorld* CurrentWorld = GetWorld();
		const UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
		if (CurrentWorld == nullptr || MovementComponent == nullptr)
		{
			return;
		}

		DeltaSeconds = CurrentWorld->DeltaTimeSeconds;
		if (DeltaSeconds == 0)
		{
			return;
		}

		CurrentAcceleration = MovementComponent->GetCurrentAcceleration();
		IsInAir = MovementComponent->IsFalling();
		MovementInputAmount = CurrentAcceleration.Size() / GetCharacterMovement()->GetMaxAcceleration();
	}
	
	const float ControlRotation = GetControlRotation().Yaw;
	const FVector Velocity = GetVelocity();
	
	MovementState = IsInAir ? EMovementState::INAIR : EMovementState::GROUNDED;
	
	FCharacterAnimInfo Info;
	Info.Speed = Velocity.Size2D();
	Info.IsMoving = Info.Speed > 1.0;
	Info.Velocity = Velocity;
	Info.Acceleration = (Velocity - PreviousVelocity) / DeltaSeconds;
	Info.AimYawRate = FMath::Abs((ControlRotation - PreviousAimYaw) / DeltaSeconds);
	Info.MovementInput = CurrentAcceleration;
	Info.AimingRotation = GetControlRotation();
	Info.MovementInputAmount = MovementInputAmount;
	Info.HasMovementInput = Info.MovementInputAmount > 0;
	Info.Gait = Gait;
	Info.MovementState = MovementState;

	PreviousVelocity = Velocity;
	PreviousAimYaw = ControlRotation;

	AnimEssentialMovementValues = Info;
}

void ABaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME_CONDITION(ABaseHuman, bJumping, COND_OwnerOnly);
	//DOREPLIFETIME_CONDITION(ABaseHuman, Energy, COND_OwnerOnly);
	//DOREPLIFETIME_CONDITION(ABaseHuman, Health, COND_OwnerOnly);
	//DOREPLIFETIME_CONDITION(ABaseHuman, bCrouching, COND_OwnerOnly);
	//DOREPLIFETIME_CONDITION(ABaseHuman, bAlive, COND_OwnerOnly);
	//DOREPLIFETIME_CONDITION(ABaseHuman, bSprinting, COND_OwnerOnly);

	DOREPLIFETIME(ABaseCharacter, SpeedScale);
	DOREPLIFETIME(ABaseCharacter, AnimEssentialMovementValues);
	DOREPLIFETIME(ABaseCharacter, Gait);
	DOREPLIFETIME(ABaseCharacter, MovementState);
}
