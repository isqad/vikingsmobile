// Fill out your copyright notice in the Description page of Project Settings.


#include "AA_AnimInstanceBase.h"
#include "Engine/World.h"
#include "BaseCharacter.h"

void UAA_AnimInstanceBase::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	if (TryGetPawnOwner())
	{
		Character = Cast<ABaseCharacter>(TryGetPawnOwner());
	}
}

void UAA_AnimInstanceBase::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	DeltaTimeX = DeltaSeconds;
}