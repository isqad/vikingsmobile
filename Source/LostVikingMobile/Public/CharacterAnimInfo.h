﻿#pragma once

#include "CoreMinimal.h"
#include "Gait.h"
#include "MovementState.h"
#include "CharacterAnimInfo.generated.h"

USTRUCT(BlueprintType)
struct FCharacterAnimInfo
{
	GENERATED_BODY()

	// Run or Walk
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EGait Gait;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EMovementState MovementState;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector Velocity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector Acceleration;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector MovementInput;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool IsMoving{ false };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool HasMovementInput{ false };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Speed{ 0.f };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float MovementInputAmount{ 0.f };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FRotator AimingRotation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float AimYawRate{ 0.f };
};
