// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MovementState.h"
#include "Animation/AnimInstance.h"
#include "AA_AnimInstanceBase.generated.h"

class ABaseCharacter;

UCLASS()
class LOSTVIKINGMOBILE_API UAA_AnimInstanceBase : public UAnimInstance
{
	GENERATED_BODY()
public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	ABaseCharacter* Character;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float DeltaTimeX;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Movement)
	FVector Velocity;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Movement)
	EMovementState MovementState;
};
