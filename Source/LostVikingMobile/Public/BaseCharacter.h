// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"

#include "Gait.h"
#include "MovementState.h"
#include "CharacterAnimInfo.h"
#include "BaseCharacter.generated.h"

class USkeletalMeshComponent;
class UCameraComponent;
class USpringArmComponent;
class UInputMappingContext;
class UInputAction;

UCLASS()
class LOSTVIKINGMOBILE_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ABaseCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform& Transform) override;

	virtual void PossessedBy(AController* NewController) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void BeginPlay() override;

#pragma region Movement
private:
	const float BaseSprintScale{ 1.f }; // 600
	const float BaseRunScale{ .5833f }; // 350
	const float BaseWalkScale{ .25f }; // 150
	
	const float MaxWalkSpeed{ 600.f };

	const float AnimatedRunSpeed{ MaxWalkSpeed*BaseRunScale };
	const float AnimatedSprintSpeed{ MaxWalkSpeed*BaseSprintScale };
	const float AnimatedWalkSpeed{ MaxWalkSpeed*BaseWalkScale };

	const FRotator DefaultRotationRate{ 0.f, 640.f, 0.f };

	UPROPERTY()
	float PreviousAimYaw{ 0.f };

	UPROPERTY()
	FVector PreviousVelocity;

	// Should be called every tick
	void CalcEssentialMovementValues();
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = Movement)
	float SpeedScale{ BaseRunScale };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = Movement)
	EGait Gait{ EGait::RUN };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = Movement)
	EMovementState MovementState{ EMovementState::GROUNDED };

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated, Category = Movement)
	FCharacterAnimInfo AnimEssentialMovementValues{};
	
#pragma endregion Movement
	
#pragma region Components
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
	USpringArmComponent* SpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	USkeletalMeshComponent* Head;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	USkeletalMeshComponent* Helmet;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Components | Body")
	USkeletalMeshComponent* Cape;
#pragma endregion Components

#pragma region Input
public:
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float LookUpMouseSensitivity{ 1.f };

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float LookRightMouseSensitivity{ 1.f };

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputMappingContext* InputMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* InventoryAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* InteractAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* LookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* SprintAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* WalkAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* CrouchAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* BlockAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* RollAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, Meta = (DisplayThumbnail = false))
	UInputAction* AttackAction;
private:
	UFUNCTION(Client, Reliable)
	void AddInputMappingContext();

	void AddInputMappingContextToController() const;

	void Input_OnLook(const FInputActionValue& ActionValue);
	void Input_Move(const FInputActionValue& ActionValue);
	void Input_OnBlock(const FInputActionValue& ActionValue);
	void Input_OnSprint(const FInputActionValue& ActionValue);
	void Input_OnJump(const FInputActionValue& ActionValue);
	void Input_OnCrouch(const FInputActionValue& ActionValue);
	void Input_OnAttack(const FInputActionValue& ActionValue);
	void Input_OnInventory(const FInputActionValue& ActionValue);
	void Input_OnInteract(const FInputActionValue& ActionValue);
#pragma endregion Input
};
