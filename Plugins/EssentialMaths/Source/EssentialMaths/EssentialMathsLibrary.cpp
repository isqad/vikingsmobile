﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "EssentialMathsLibrary.h"

inline FVector2D UEssentialMathsLibrary::ClampMagnitude012D(const FVector2D& Vector)
{
	const float MagnitudeSquared = Vector.SizeSquared();

	if (MagnitudeSquared <= 1.0f)
	{
		return Vector;
	}

	const float Scale = FMath::InvSqrt(MagnitudeSquared);

	return { Vector.X * Scale, Vector.Y * Scale };
}

inline FVector UEssentialMathsLibrary::RadianToDirection3D(const float Radian)
{
	float Sin, Cos;
	FMath::SinCos(&Sin, &Cos, Radian);

	return { Cos, Sin, 0.0f };
}

inline FVector UEssentialMathsLibrary::AngleToDirection2D(const float Angle)
{
	return UEssentialMathsLibrary::RadianToDirection3D(FMath::DegreesToRadians(Angle));
}

inline FVector UEssentialMathsLibrary::RightVectorFromVector(const FVector& Vector)
{
	return { -Vector.Y, Vector.X, Vector.Z };
}
