﻿#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "EssentialMathsLibrary.generated.h"

UCLASS()
class ESSENTIALMATHS_API UEssentialMathsLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "Essential Maths|Vector", DisplayName = "Clamps magnitude",
		Meta = (AutoCreateRefTerm = "Vector", ReturnDisplayName = "Vector"))
	static FVector2D ClampMagnitude012D(const FVector2D& Vector);

	UFUNCTION(BlueprintPure, Category = "Essential Maths|Vector", DisplayName = "Radian To Direction 3D",
		Meta = (AutoCreateRefTerm = "Vector", ReturnDisplayName = "Vector"))
	static FVector RadianToDirection3D(const float Radian);

	UFUNCTION(BlueprintPure, Category = "Essential Maths|Vector", DisplayName = "Angle To Direction 2D",
		Meta = (AutoCreateRefTerm = "Vector", ReturnDisplayName = "Vector"))
	static FVector AngleToDirection2D(const float Angle);

	UFUNCTION(BlueprintPure, Category = "Essential Maths|Vector", DisplayName = "Right Vector From Vector",
		Meta = (AutoCreateRefTerm = "Vector", ReturnDisplayName = "Vector"))
	static FVector RightVectorFromVector(const FVector& Vector);
};
